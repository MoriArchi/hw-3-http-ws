import * as config from './config';
import useMiddleware from './middleware';
import {
  getRoomEntity,
  roomExists,
  sendRoomsList,
  updateRoom,
  joinSocketToRoom,
  clearRoom,
  toggleRoomAvailabilityIfNeeds
} from './helpers/roomHelper';

import {
  generateWinnersList,
  scheduleGameIfNeeds,
  checkRoomForEnding
} from './helpers/gameHelper';

import { Game } from '../models/game';

export const availableRooms = {}

export default io => {
  useMiddleware(io);

  const updateRoomIfExists = (io, roomName) => {
    if (roomExists(io, roomName)) {
      updateRoom(io, roomName);
    }
  };

  io.on('connection', socket => {
    const username = socket.handshake.query.username;

    socket.userData = {
      name: username,
      ready: false,
      progress: 0,
      lastPress: null,
      typed: 0
    };

    sendRoomsList(io, socket);

    socket.on('disconnect', () => {
      const { roomName } = socket;

      updateRoomIfExists(io, roomName);

      toggleRoomAvailabilityIfNeeds(io, roomName);

      sendRoomsList(io, socket.broadcast);
      scheduleGameIfNeeds(io, roomName);
      checkRoomForEnding(io, roomName, socket);
    });

    socket.on('NEW_ROOM', (roomName, callback) => {
      if (roomExists(io, roomName)) {
        callback(`Room with name ${roomName} already exists`);
        return;
      }

      joinSocketToRoom(io, socket, roomName, callback);
      availableRooms[roomName] = true;

      const room = getRoomEntity(io, roomName);

      room.name = roomName;

      room.game = new Game({
        onGameEnd: (gameStartedAt) => {
          io.in(roomName).emit('END_GAME', generateWinnersList(io, roomName, gameStartedAt));
          clearRoom(io, roomName);
          toggleRoomAvailabilityIfNeeds(io, roomName);
          sendRoomsList(io, socket.broadcast);
        },
        onUpdateCountdown: (countdown) => {
          io.in(roomName).emit('UPDATE_COUNTDOWN', countdown);
        },
        onStartGame: (text) => {
          io.in(roomName).emit('START_GAME', text);
        },
        onUpdateGameTimer: (secsToGameEnd) => {
          io.in(roomName).emit('UPDATE_GAME_TIMER', secsToGameEnd);
        },
      });

    });

    socket.on('JOIN_ROOM', ({ roomName }, callback) => {
      const roomLength = getRoomEntity(io, roomName).length;

      toggleRoomAvailabilityIfNeeds(io, roomName);

      if (roomLength === config.MAXIMUM_USERS_FOR_ONE_ROOM) {
        callback('Maximum number of players in the room is reached');
        return;
      }

      joinSocketToRoom(io, socket, roomName, callback);
    });

    socket.on('LEAVE_ROOM', () => {
      const roomName = socket.roomName;

      socket.leave(roomName, () => {
        socket.roomName = null;

        if (roomExists(io, roomName)) {
          updateRoom(io, roomName);
          scheduleGameIfNeeds(io, roomName);
        }

        toggleRoomAvailabilityIfNeeds(io, roomName);

        sendRoomsList(io, io);
      });
    });

    socket.on('TOGGLE_READY', () => {
      socket.userData.ready = !socket.userData.ready;

      updateRoomIfExists(io, socket.roomName);
      scheduleGameIfNeeds(io, socket.roomName);
    });

    socket.on('USER_TYPING', (key) => {
      const { roomName, userData } = socket;
  
      const { text } = getRoomEntity(io, roomName).game;

      const typedLettersAmount = userData.typed;

      if (text.length === typedLettersAmount) {
        return;
      }

      if (text[typedLettersAmount] === key) {
        socket.userData.lastPress = Date.now();
        socket.userData.typed++;
        socket.userData.progress = socket.userData.typed / text.length;

        io.in(roomName).emit('UPDATE_PROGRESS', socket.userData.name, socket.userData.progress);
  
        const typedText = text.substring(0, socket.userData.typed);

        const nextLetter = text.length === socket.userData.typed
          ? ''
          : text[socket.userData.typed];

        const textLeft = text.length > socket.userData.typed + 1
          ? text.substring(socket.userData.typed + 1)
          : '';

        socket.emit('UPDATE_USER_TYPED_TEXT', { typedText, nextLetter, textLeft });

        checkRoomForEnding(io, roomName, socket)
      }
    })

  });
};